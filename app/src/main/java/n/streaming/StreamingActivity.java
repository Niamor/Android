package n.streaming;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

import n.streaming.core.StreamingStatus;
import n.streaming.core.bluetooth.BluetoothManager;
import n.streaming.core.os.FileHelper;

public class StreamingActivity extends AppCompatActivity {
    private MediaController controller;

    VideoView videoView = null;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context,
                              Intent intent) {
            String action = intent.getAction();
            if (action == null) {
                return;
            }

            if (action.equals("startPlayingStreamingVideo")) {
                setSourceAndStartPlay(StreamingStatus.GetInstance().GetTemporaryFile());
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_streaming);

        videoView = (VideoView) findViewById(R.id.video);


        controller = new MediaController(this);

        controller.setMediaPlayer(videoView);
        videoView.setMediaController(controller);
        videoView.requestFocus();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                videoView.seekTo(StreamingStatus.GetInstance().GetPosition());
                if (StreamingStatus.GetInstance().GetPosition() == 0) {
                    videoView.start();
                }

                // When video Screen change size.
                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {

                        // Re-Set the videoView that acts as the anchor for the MediaController
                        controller.setAnchorView(videoView);
                    }
                });
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        StreamingStatus.GetInstance().SetPosition(mp.getCurrentPosition());
                        try {
                            mp.reset();
                            videoView.setVideoPath(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "P2PStreaming/"
                                    + StreamingStatus.GetInstance().GetTemporaryFile()).getAbsolutePath());
                            mp.seekTo(StreamingStatus.GetInstance().GetPosition());
                            videoView.start();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        });

        IntentFilter filter = new IntentFilter();
        filter.addAction("startPlayingStreamingVideo");
        registerReceiver(broadcastReceiver,
                filter);

        new GetVideoFile().start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        StreamingStatus.GetInstance().Clear();
        unregisterReceiver(broadcastReceiver);
    }

    private class GetVideoFile extends Thread {
        private String mUrl;
        private String mFile;

        public GetVideoFile() {

        }

        @Override
        public void run() {
            super.run();
            if (!StreamingStatus.GetInstance().CreateTempFile())
                return;
            try {
                BufferedOutputStream bufferOS = new BufferedOutputStream(new FileOutputStream(StreamingStatus.GetInstance().GetTemporaryFile()));

                BufferedInputStream bis = new BufferedInputStream(StreamingStatus.GetInstance().GetInputStream(), 2048);

                byte[] buffer = new byte[BluetoothManager.BUFFER_LENGTH];
                int numRead;
                boolean started = false;
                while ((numRead = bis.read(buffer)) != -1) {
                    bufferOS.write(buffer, 0, numRead);
                    bufferOS.flush();
                    StreamingStatus.GetInstance().IncrementTotalReadSize(numRead);
                    if (StreamingStatus.GetInstance().GetTotalReadSize() > 500000 &&
                            !started) {
                        Log.d("Player", "BufferHIT:StartPlay");
                        Intent intent = new Intent("startPlayingStreamingVideo");
                        StreamingActivity.this.sendBroadcast(intent);
                        started = true;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setSourceAndStartPlay(File bufferFile) {
        try {
            StreamingStatus.GetInstance().SetPosition(videoView.getCurrentPosition());
            videoView.setVideoPath(bufferFile.getAbsolutePath());

            videoView.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onCompletion(MediaPlayer mp) {
        StreamingStatus.GetInstance().SetPosition(mp.getCurrentPosition());
        try {
            mp.reset();
            videoView.setVideoPath(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "P2PStreaming/"
                    + StreamingStatus.GetInstance().GetTemporaryFile()).getAbsolutePath());
            mp.seekTo(StreamingStatus.GetInstance().GetPosition());
            videoView.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        StreamingStatus.GetInstance().Clear();
    }
}