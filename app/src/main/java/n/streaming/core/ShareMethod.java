package n.streaming.core;

public enum ShareMethod {
    SHARE_METHOD_DIRECT_DOWNLOAD,
    SHARE_METHOD_STREAMING;
}
