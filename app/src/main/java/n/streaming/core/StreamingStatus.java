package n.streaming.core;

import android.bluetooth.BluetoothSocket;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class StreamingStatus {
    // static instance
    private static StreamingStatus instance = new StreamingStatus();

    // total size read in stream
    private long TotalRead = 0;

    // video position
    private int VideoPosition = 0;

    // buffer output stream
    private File BufferFile;

    // network input stream
    private InputStream inputStream;

    // bluetooth socket
    private BluetoothSocket Socket;

    // get instance
    public static StreamingStatus GetInstance() {
        return instance;
    }

    // build streaming status
    private StreamingStatus() {

    }

    // create temp file
    public boolean CreateTempFile() {
        try {
            BufferFile = File.createTempFile("test",
                    "mp4");
            BufferFile.deleteOnExit();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    // get temp file
    public File GetTemporaryFile() {
        return BufferFile;
    }

    // set input stream
    public void SetInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    // get temp file
    public InputStream GetInputStream() {
        return this.inputStream;
    }

    // increment total read size
    public void IncrementTotalReadSize(int size) {
        TotalRead += size;
    }

    // get total read size
    public long GetTotalReadSize() {
        return TotalRead;
    }

    // clear
    public void Clear() {
        if (this.Socket != null) {
            try {
                this.Socket.close();
            } catch (IOException e) {

            }
        }
        this.TotalRead = 0;
        this.VideoPosition = 0;
    }

    // set video position
    public void SetPosition(int position) {
        this.VideoPosition = position;
    }

    // get video position
    public int GetPosition() {
        return this.VideoPosition;
    }

    // set socket
    public void SetSocket(BluetoothSocket socket) {
        this.Socket = socket;
    }
}
