package n.streaming.core;

import android.support.design.widget.Snackbar;

import n.streaming.ClientActivity;
import n.streaming.core.bluetooth.BluetoothClientManager;

public class ClientManager {
    // singleton
    static private ClientManager instance = new ClientManager();

    // connection type
    private ConnectionType connectionType;

    // current state
    private int currentStatus;

    // get client manager
    static public ClientManager GetInstance() {
        return instance;
    }

    // build client manager
    private ClientManager() {

    }

    // reset connection
    public void ResetConnection() {
        this.currentStatus = 0;
    }

    // get status
    public int GetStatus() {
        return this.currentStatus;
    }

    // get connection type
    public ConnectionType GetConnectionType() {
        return this.connectionType;
    }

    // set connection type
    public void SetConnectionType(ConnectionType connectionType) {
        // reset connection
        this.ResetConnection();

        // save new connection mode
        this.connectionType = connectionType;
    }

    // look for server
    public boolean LookForServer(ClientActivity activity,
                                 ConnectionType connectionType,
                                 Snackbar snackbar) {
        // process request
        switch (connectionType) {
            case CONNECTION_TYPE_BLUETOOTH:
                // notify
                activity.SetSnackbarMessage("enabling bluetooth...");

                // set state
                this.currentStatus = 1;

                // pop up to enable bluetooth if not already enabled
                BluetoothClientManager.GetInstance().RequestEnableBluetooth(activity);
                break;
            case CONNECTION_TYPE_WIFI:
                activity.SetSnackbarMessage("Not implemented...");
                break;

            default:
                snackbar.dismiss();
                activity.EditConnectionButton(true);
                return false;
        }

        // ok
        return true;
    }
}
