package n.streaming.core.os;

public class Storage {
    private String VideoPath;

    private static Storage instance = new Storage();

    public Storage() {
        VideoPath = FileHelper.getAppDataDirectory() + "P2PVideo";
    }

    public void setVideoPath(String path) {
        VideoPath = path;
    }

    public String getVideoPath() {
        return VideoPath;
    }

    public static Storage GetInstance() {
        return instance;
    }
}
