package n.streaming.core;

public enum ConnectionType {
    // connection types
    CONNECTION_TYPE_WIFI,
    CONNECTION_TYPE_BLUETOOTH;

    // names associated to connection types
    public static String[] ConnectionTypeSSID = {
            "NProWifi",
            "NProBlue"
    };

    // uuid associated to connection types
    public static String[] ConnectionTypeUUID = {
            "00000000-0000-0000-0000-000000000000",
            "ABCDEFAB-0000-0000-0000-CCCABAB1E1FF"
    };
}
