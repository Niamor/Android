package n.streaming.core;

import android.support.design.widget.Snackbar;

import n.streaming.ServerActivity;
import n.streaming.core.bluetooth.BluetoothServerManager;
import n.streaming.core.os.Storage;

public class ServerManager {
    // singleton instance
    private static ServerManager instance = new ServerManager();

    // get singleton instance
    public static ServerManager GetInstance() {
        return ServerManager.instance;
    }

    // current status
    private int currentStatus;

    // build server manager
    private ServerManager() {

    }

    // start server
    public void StartServer(ServerActivity activity,
                            ConnectionType connectionType,
                            ShareMethod shareMethod,
                            Snackbar snackbar) {
        BluetoothServerManager.GetInstance().SetShareMethod(shareMethod);
        switch (connectionType) {
            case CONNECTION_TYPE_WIFI:
                activity.SetSnackbarMessage("Not implemented...");
                break;
            case CONNECTION_TYPE_BLUETOOTH:
                // notify
                activity.SetSnackbarMessage("enabling bluetooth...");

                // set state
                this.currentStatus = 1;

                // enable bluetooth
                BluetoothServerManager.GetInstance().RequestEnableBluetooth(activity);
                break;

            default:
                snackbar.dismiss();
                activity.EditConnectionButton(true);
                break;
        }
    }

    // get status
    public int GetStatus() {
        return this.currentStatus;
    }
}
