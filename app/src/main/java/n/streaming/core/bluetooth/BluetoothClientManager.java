package n.streaming.core.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;

import n.streaming.ClientActivity;
import n.streaming.StreamingActivity;
import n.streaming.core.ConnectionType;
import n.streaming.core.RequestCode;
import n.streaming.core.ShareMethod;
import n.streaming.core.StreamingStatus;
import n.streaming.core.os.FileHelper;

public class BluetoothClientManager extends BluetoothManager {
    // instance
    private static BluetoothClientManager instance = new BluetoothClientManager();

    // bluetooth devices list
    private HashMap<String, BluetoothDevice> deviceList = new HashMap<>();

    // build bluetooth manager
    private BluetoothClientManager() {
        super();
    }

    // get instance
    public static BluetoothClientManager GetInstance() {
        return instance;
    }

    // receiver class
    private class BroadcastReceiverActivity extends BroadcastReceiver {
        // client activity
        private ClientActivity activity;

        // set activity address
        public void SetClientActivityAddress(ClientActivity activity) {
            this.activity = activity;
        }

        @Override
        public void onReceive(Context context,
                              Intent intent) {
            // get intent
            String action = intent.getAction();

            // check action
            if (action == null)
                return;

            // analyze action type
            if (action.equals(BluetoothDevice.ACTION_FOUND)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                BluetoothClientManager.this.deviceList.put(device.getAddress(),
                        device);
                activity.SetSnackbarMessage("Found " +
                        device.getAddress() +
                        (device.getName() != null ?
                                (" (" +
                                        device.getName() +
                                        ")")
                                : ""));
            } else if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)) {
                activity.CatchEvent(RequestCode.REQUEST_CODE_BLUETOOTH_DISCOVERY_DONE.ordinal(),
                        Activity.RESULT_OK,
                        null);
            }
        }
    }

    // broadcast receiver for bluetooth device detection
    private final BroadcastReceiverActivity broadcastReceiver = new BroadcastReceiverActivity();

    // get broadcast receiver
    public BroadcastReceiverActivity GetBroadcastReceiver() {
        return broadcastReceiver;
    }

    // get devices list
    public final HashMap<String, BluetoothDevice> GetDeviceList() {
        return this.deviceList;
    }

    // start lookup
    public void StartDiscovery(ClientActivity activity) {
        // save activity address
        broadcastReceiver.SetClientActivityAddress(activity);

        // clear devices list
        this.deviceList.clear();

        // start devices discovery
        super.adapter.cancelDiscovery();
        super.adapter.startDiscovery();
    }

    private class ConnectThread extends Thread {
        private ClientActivity clientActivity;
        private BluetoothDevice device;
        private BluetoothSocket socket;
        private boolean isReceive = true;

        public ConnectThread(BluetoothDevice device,
                             ClientActivity activity) {
            BluetoothSocket tmp = null;
            this.device = device;
            this.clientActivity = activity;
            try {
                tmp = device.createRfcommSocketToServiceRecord(UUID.fromString(ConnectionType.ConnectionTypeUUID[ConnectionType.CONNECTION_TYPE_BLUETOOTH.ordinal()]));
            } catch (IOException e) {

            }
            socket = tmp;
        }

        public void run() {
            adapter.cancelDiscovery();
            try {
                if (socket != null)
                    socket.connect();
            } catch (IOException connectException) {
                try {
                    socket.close();
                } catch (IOException closeException) {

                }
                return;
            }

            // notify connection
            SetSnackbarMessage("Now connected to " +
                            socket.getRemoteDevice().getAddress(),
                    false);

            // read file
            try {
                // get input stream
                InputStream is = socket.getInputStream();

                // create app data directory
                FileHelper.createAppDataDirectory();

                // open output file
                File outputFile = new File(FileHelper.getAppDataDirectory() +
                        "Download.mp4");
                FileOutputStream fos = new FileOutputStream(outputFile);

                // read sharing method
                byte[] rawSharingMethod = new byte[4];
                if (is.read(rawSharingMethod) != 4) {
                    SetSnackbarMessage("Can't receive sharing method",
                            true);
                    fos.close();
                    socket.close();
                    return;
                }
                ByteBuffer wrapped = ByteBuffer.wrap(rawSharingMethod);
                int shareMethodOrdinal = wrapped.getInt();
                if (shareMethodOrdinal >= ShareMethod.values().length) {
                    SetSnackbarMessage("Invalid sharing method",
                            true);
                    fos.close();
                    socket.close();
                    return;
                }
                ShareMethod shareMethod = ShareMethod.values()[shareMethodOrdinal];

                // read size
                byte[] rawSize = new byte[4];
                if (is.read(rawSize) != 4) {
                    SetSnackbarMessage("Can't receive file size",
                            true);
                    fos.close();
                    socket.close();
                    return;
                }
                int size;
                wrapped = ByteBuffer.wrap(rawSize);
                size = wrapped.getInt();
                SetSnackbarMessage("File size is " +
                                size +
                                " bytes",
                        false);

                switch (shareMethod) {
                    case SHARE_METHOD_STREAMING:
                        // clear streaming status
                        StreamingStatus.GetInstance().Clear();

                        // set input stream
                        StreamingStatus.GetInstance().SetSocket(socket);
                        StreamingStatus.GetInstance().SetInputStream(is);

                        // display toast
                        Intent intent = new Intent("broadcastClientManagerToastMessage");
                        intent.putExtra("message",
                                "Streaming...");
                        this.clientActivity.sendBroadcast(intent);

                        // start streaming activity
                        intent = new Intent(this.clientActivity,
                                StreamingActivity.class);
                        this.clientActivity.startActivity(intent);
                        break;

                    case SHARE_METHOD_DIRECT_DOWNLOAD:
                        // receive file
                        byte[] buffer = new byte[BluetoothManager.BUFFER_LENGTH];
                        int cursor = 0;
                        for (; cursor < size && isReceive; ) {
                            int readSize = is.read(buffer);
                            if (readSize != -1) {
                                // write into file
                                fos.write(Arrays.copyOfRange(buffer,
                                        0,
                                        readSize));

                                // edit message
                                SetSnackbarMessage(((int) ((float) cursor / (float) size * 100.0f)) +
                                                "% - (" +
                                                cursor +
                                                "/" +
                                                size +
                                                ")",
                                        false);

                                // increment cursor
                                cursor += readSize;
                            } else {
                                break;
                            }
                        }

                        // notify
                        if (cursor >= size) {
                            SetSnackbarMessage("Done! (" +
                                            FileHelper.getAppDataDirectory() +
                                            "Download.mp4" +
                                            ")",
                                    true);
                            clientActivity.EditConnectionButton(true);
                        } else {
                            SetSnackbarMessage("Failed to download file.",
                                    true);
                        }
                        socket.close();
                        break;

                    default:
                        break;
                }

                // close
                fos.close();
            } catch (IOException e) {
                SetSnackbarMessage("Operation failed: " +
                                e.toString(),
                        true);
            }
        }

        // edit snackbar message
        public void SetSnackbarMessage(String message,
                                       boolean isClosable) {
            Intent intent = new Intent("broadcastClientManagerMessage");
            intent.putExtra("message",
                    message);
            intent.putExtra("isClosable",
                    isClosable);
            this.clientActivity.sendBroadcast(intent);
        }

        public void cancel() {
            isReceive = false;
            try {
                socket.close();
                socket = null;
            } catch (IOException e) {
            }
        }

    }

    // connection thread
    private ConnectThread connectThread;

    // process found devices list
    public void ProcessDeviceResult(ClientActivity activity,
                                    HashMap<String, BluetoothDevice> deviceList) {
        for (BluetoothDevice device : deviceList.values()) {
            if (device.getName() != null &&
                    device.getName().equals(ConnectionType.ConnectionTypeSSID[ConnectionType.CONNECTION_TYPE_BLUETOOTH.ordinal()])) {
                activity.SetSnackbarMessage("Now connecting to " +
                        device.getAddress() +
                        "...");
                connectThread = new ConnectThread(device,
                        activity);
                connectThread.start();
            }
        }
    }
}


