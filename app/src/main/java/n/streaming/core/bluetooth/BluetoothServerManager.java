package n.streaming.core.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import n.streaming.ServerActivity;
import n.streaming.core.ConnectionType;
import n.streaming.core.RequestCode;
import n.streaming.core.ShareMethod;
import n.streaming.core.os.Storage;

public class BluetoothServerManager extends BluetoothManager {
    // singleton instance
    private static BluetoothServerManager instance = new BluetoothServerManager();

    // get singleton instance
    public static BluetoothServerManager GetInstance() {
        return BluetoothServerManager.instance;
    }

    // socket list
    private ArrayList<BluetoothSocket> socketList = new ArrayList<>();

    // file sender thread list
    private ArrayList<FileSenderThread> threadList = new ArrayList<>();

    // share method
    private ShareMethod shareMethod;

    // file sender thread class
    private class FileSenderThread extends Thread {
        private BluetoothSocket socket;
        private boolean isSend = true;
        private ServerActivity activity;

        public FileSenderThread(BluetoothSocket socket,
                                ServerActivity activity) {
            this.socket = socket;
            this.activity = activity;
        }

        @Override
        public void run() {
            // get path
            String path = Storage.GetInstance().getVideoPath();

            // check path
            if (path == null) {
                return;
            }

            try {
                // open file
                File file = new File(path);
                FileInputStream fis = new FileInputStream(file);

                // get output stream of socket
                OutputStream os = socket.getOutputStream();

                // write sharing method
                os.write(ByteBuffer.allocate(4).putInt(shareMethod.ordinal()).array());

                // write file length
                os.write(ByteBuffer.allocate(4).putInt((int) file.length()).array());

                // write file into socket
                byte[] buffer = new byte[BluetoothManager.BUFFER_LENGTH];
                for (int cursor = 0; cursor < file.length() && isSend; ) {
                    int length = fis.read(buffer);
                    if (length != -1) {
                        os.write(Arrays.copyOfRange(buffer,
                                0,
                                length));
                        cursor += length;
                    } else {
                        break;
                    }
                }

                // notify end
                SetSnackbarMessage("Done sending file!",
                        false);

                // close file
                fis.close();
            } catch (IOException e) {
                SetSnackbarMessage("Failed to send: " +
                                e.toString(),
                        false);
            }
        }

        // edit snackbar message
        public void SetSnackbarMessage(String message,
                                       boolean isClosale) {
            Intent intent = new Intent("broadcastServerManagerMessage");
            intent.putExtra("message",
                    message);
            intent.putExtra("isClosable",
                    isClosale);
            this.activity.sendBroadcast(intent);
        }

        public void cancel() {
            isSend = false;
        }
    }

    private class AcceptThread extends Thread {
        private final BluetoothServerSocket mmServerSocket;
        private ServerActivity activity;

        public AcceptThread() {
            BluetoothServerSocket tmp = null;
            try {
                tmp = adapter.listenUsingRfcommWithServiceRecord(ConnectionType.ConnectionTypeSSID[ConnectionType.CONNECTION_TYPE_BLUETOOTH.ordinal()],
                        UUID.fromString(ConnectionType.ConnectionTypeUUID[ConnectionType.CONNECTION_TYPE_BLUETOOTH.ordinal()]));
            } catch (IOException e) {
            }
            mmServerSocket = tmp;
        }

        // set server activity
        public void SetServerActivity(ServerActivity serverActivity) {
            this.activity = serverActivity;
        }

        public void run() {
            BluetoothSocket socket = null;
            while (true) {
                try {
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    break;
                }

                if (socket != null) {
                    // add socket to known sockets
                    socketList.add(socket);

                    // notify
                    Intent intent = new Intent("broadcastServerManagerMessage");
                    intent.putExtra("message",
                            socket.getRemoteDevice().getAddress() +
                                    " just connected!");
                    this.activity.sendBroadcast(intent);

                    // start upload job
                    FileSenderThread senderThread = new FileSenderThread(socket,
                            this.activity);
                    threadList.add(senderThread);
                    senderThread.start();

                    // notify
                    intent = new Intent("broadcastServerManagerMessage");
                    intent.putExtra("message",
                            "Now sending file to " +
                                    socket.getRemoteDevice().getAddress());
                    this.activity.sendBroadcast(intent);
                    break;
                }
            }
        }

        public void cancel() {
            try {
                mmServerSocket.close();
            } catch (IOException e) {
            }
        }
    }

    // accept thread
    private AcceptThread acceptThread;

    // build bluetooth server manager
    private BluetoothServerManager() {
        // parent constructor
        super();
    }

    // enable bluetooth listening mode
    public void ActivateServer(ServerActivity activity) {
        // set adapter name
        super.adapter.setName(ConnectionType.ConnectionTypeSSID[ConnectionType.CONNECTION_TYPE_BLUETOOTH.ordinal()]);

        // start accept thread
        KillAll();
        if (acceptThread != null) {
            acceptThread.cancel();
        }
        acceptThread = new AcceptThread();
        acceptThread.SetServerActivity(activity);
        acceptThread.start();

        // make device discoverable
        Intent i = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        i.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION,
                3600);
        activity.startActivityForResult(i,
                RequestCode.REQUEST_CODE_BLUETOOTH_MAKE_DISCOVERABLE.ordinal());
    }

    // kill all clients
    public void KillAll() {
        for (FileSenderThread thread : this.threadList) {
            thread.cancel();
        }

        for (BluetoothSocket socket : this.socketList) {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
        this.socketList.clear();
    }

    // set share method
    public void SetShareMethod(ShareMethod shareMethod) {
        this.shareMethod = shareMethod;
    }
}
