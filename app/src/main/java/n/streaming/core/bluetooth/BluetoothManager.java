package n.streaming.core.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;

import n.streaming.core.RequestCode;

public class BluetoothManager {
    // buffer length
    public static final int BUFFER_LENGTH = 8192;

    // is bluetooth usable?
    private boolean isUsable;

    // bluetooth adapter
    protected BluetoothAdapter adapter;

    // build
    protected BluetoothManager() {
        // check if bluetooth is usable
        this.isUsable = (this.adapter = BluetoothAdapter.getDefaultAdapter()) != null;
    }

    // is bluetooth usable?
    public boolean IsBluetoothUsable() {
        return this.isUsable;
    }

    // is bluetooth enabled?
    public boolean IsBluetoothEnabled() {
        return this.adapter.isEnabled();
    }

    // enable bluetooth
    public void RequestEnableBluetooth(Activity currentActivity) {
        // request bluetooth activation
        Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        currentActivity.startActivityForResult(enableBT,
                RequestCode.REQUEST_CODE_ENABLE_BLUETOOTH.ordinal());
    }
}
