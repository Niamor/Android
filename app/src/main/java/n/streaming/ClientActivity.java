package n.streaming;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.HashMap;

import n.streaming.core.ClientManager;
import n.streaming.core.ConnectionType;
import n.streaming.core.RequestCode;
import n.streaming.core.bluetooth.BluetoothClientManager;

public class ClientActivity extends AppCompatActivity {
    // snackbar for status updates
    private Snackbar snackbar = null;

    // broadcast receiver class
    private class BroadcastReceiverActivity extends BroadcastReceiver {
        @Override
        public void onReceive(Context context,
                              Intent intent) {
            // get intent
            String action = intent.getAction();

            // check action
            if (action == null)
                return;

            // analyze action
            if (action.equals("broadcastClientManagerMessage")) {
                if (intent.getBooleanExtra("isClosable",
                        false)) {
                    SetClosableSnackbar(intent.getStringExtra("message"));
                } else {
                    SetSnackbarMessage(intent.getStringExtra("message"));
                }
            } else if (action.equals("broadcastClientManagerToastMessage")) {
                Toast.makeText(ClientActivity.this,
                        intent.getStringExtra("message"),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    // broadcast receiver
    private BroadcastReceiverActivity broadcastReceiver;

    @Override
    public void onStart() {
        // parent start
        super.onStart();

        // create intent filter
        IntentFilter filter = new IntentFilter();

        // add bluetooth intent capture
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

        // register receiver to client activity
        this.registerReceiver(BluetoothClientManager.GetInstance().GetBroadcastReceiver(),
                filter);

        // create broadcast receiver
        this.broadcastReceiver = new BroadcastReceiverActivity();

        // create intent filter
        filter = new IntentFilter();

        // add custom intent
        filter.addAction("broadcastClientManagerMessage");
        filter.addAction("broadcastClientManagerToastMessage");

        // register
        registerReceiver(this.broadcastReceiver,
                filter);
    }

    @Override
    public void onStop() {
        // parent stop
        super.onStop();

        // unregister
        this.unregisterReceiver(BluetoothClientManager.GetInstance().GetBroadcastReceiver());
        this.unregisterReceiver(this.broadcastReceiver);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // parent constructor
        super.onCreate(savedInstanceState);

        // set view
        setContentView(R.layout.activity_client);

        // parameter toolbar
        Toolbar toolbar = findViewById(R.id.activity_client_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // build snackbar
        final FloatingActionButton floatingButton = findViewById(R.id.client_activity_connect_floating_button);
        snackbar = Snackbar.make(floatingButton,
                "",
                Snackbar.LENGTH_INDEFINITE);

        // parameter floating button for connection
        floatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // set connection type
                snackbar.show();

                // start looking for server
                ClientManager.GetInstance().LookForServer(ClientActivity.this,
                        ((RadioButton) findViewById(R.id.activity_client_connection_type_radio_wifi)).isChecked() ?
                                ConnectionType.CONNECTION_TYPE_WIFI
                                : ConnectionType.CONNECTION_TYPE_BLUETOOTH,
                        snackbar);

                if (!((RadioButton) findViewById(R.id.activity_client_connection_type_radio_wifi)).isChecked()) {
                    // disable button
                    ClientActivity.this.EditConnectionButton(false);
                }
            }
        });

        // parameter read video button
        Button readVideoButton = findViewById(R.id.activity_client_read_download_video_button);
        readVideoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ClientActivity.this, VideoActivity.class);
                startActivity(intent);
            }
        });
    }

    // on resume
    @Override
    public void onResume() {
        // parent resume
        super.onResume();
    }

    // on pause
    @Override
    public void onPause() {
        // parent pause
        super.onPause();
    }

    // on destroy
    @Override
    public void onDestroy() {
        // destroy parent
        super.onDestroy();
    }

    // catch event
    public void CatchEvent(int requestCode,
                           int resultCode,
                           Intent data) {
        // check request code
        if (requestCode >= RequestCode.values().length) {
            ClientManager.GetInstance().ResetConnection();
            return;
        }

        // process request
        switch (RequestCode.values()[requestCode]) {
            case REQUEST_CODE_ENABLE_BLUETOOTH:
                if (resultCode == RESULT_OK) {
                    if (ClientManager.GetInstance().GetStatus() == 1) {
                        // now looking for devices
                        this.SetSnackbarMessage("Looking for devices...");

                        // start discovery
                        BluetoothClientManager.GetInstance().StartDiscovery(this);
                    }
                } else {
                    this.EditConnectionButton(true);
                    this.SetClosableSnackbar("Bluetooth activation failed");
                }
                break;
            case REQUEST_CODE_BLUETOOTH_DISCOVERY_DONE:
                // get found bluetooth devices list
                HashMap<String, BluetoothDevice> deviceList = BluetoothClientManager.GetInstance().GetDeviceList();

                // notify snackbar
                this.SetSnackbarMessage(deviceList.size() +
                        " devices have been found...");

                // process devices list
                BluetoothClientManager.GetInstance().ProcessDeviceResult(this,
                        deviceList);

                // activate connection button
                this.EditConnectionButton(true);
                break;

            default:
                break;
        }
    }

    // catch activity result
    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data) {
        CatchEvent(requestCode,
                resultCode,
                data);
    }

    // configure snackbar to be closable
    private void SetClosableSnackbar(String message) {
        this.snackbar.setText(message);
        this.snackbar.setAction("Close",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                }
        );
        this.snackbar.show();
    }

    // set snackbar message
    public void SetSnackbarMessage(String message) {
        this.snackbar.setText(message);
        this.snackbar.setAction("Close",
                null);
    }

    // enable/disable connection button
    public void EditConnectionButton(boolean isEnabled) {
        FloatingActionButton button = findViewById(R.id.client_activity_connect_floating_button);
        button.setEnabled(isEnabled);
    }
}
