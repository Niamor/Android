package n.streaming;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.VideoView;

import n.streaming.core.os.FileHelper;
import n.streaming.core.os.Storage;

public class VideoActivity extends AppCompatActivity {

    private static int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        // Get a reference to the VideoView instance as follows, using the id we set in the XML layout.
        VideoView vidView = (VideoView) findViewById(R.id.video);

        // Add playback controls.
        MediaController vidControl = new MediaController(this);
        // Set it to use the VideoView instance as its anchor.
        vidControl.setAnchorView(vidView);
        // Set it as the media controller for the VideoView object.
        vidView.setMediaController(vidControl);

        // Prepare the URI for the endpoint.
        System.out.println(Storage.GetInstance().getVideoPath());
        String vidAddress = FileHelper.getAppDataDirectory() +
                "Download.mp4";
        Uri vidUri = Uri.parse("file://" + vidAddress);
        // Parse the address string as a URI so that we can pass it to the VideoView object.
        vidView.setVideoURI(vidUri);
        // Start playback.
        vidView.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        VideoView vidView = (VideoView) findViewById(R.id.video);
        vidView.pause();
        position = vidView.getCurrentPosition();
    }

    @Override
    protected void onResume() {
        super.onResume();
        VideoView vidView = (VideoView) findViewById(R.id.video);
        vidView.seekTo(position);
        vidView.start();
    }
}
