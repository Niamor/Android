package n.streaming;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;

import java.io.File;

import n.streaming.core.ClientManager;
import n.streaming.core.ConnectionType;
import n.streaming.core.RequestCode;
import n.streaming.core.ServerManager;
import n.streaming.core.ShareMethod;
import n.streaming.core.bluetooth.BluetoothServerManager;


public class ServerActivity extends AppCompatActivity {
    // snackbar for status updates
    private Snackbar snackbar = null;

    // broadcast receiver
    private BroadcastReceiverActivity broadcastReceiver;

    // broadcast receiver class
    private class BroadcastReceiverActivity extends BroadcastReceiver {
        @Override
        public void onReceive(Context context,
                              Intent intent) {
            // get intent
            String action = intent.getAction();

            // check action
            if (action == null)
                return;

            // analyze action
            if (action.equals("broadcastServerManagerMessage")) {
                SetSnackbarMessage(intent.getStringExtra("message"));
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        // set view
        setContentView(R.layout.activity_server);

        // parameter toolbar
        Toolbar toolbar = findViewById(R.id.activity_server_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // build snackbar
        FloatingActionButton floatingButton = findViewById(R.id.server_activity_connect_floating_button);
        snackbar = Snackbar.make(floatingButton,
                "",
                Snackbar.LENGTH_INDEFINITE);

        // parameter floating button for connection
        floatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // set connection type
                snackbar.show();

                // activate server
                ServerManager.GetInstance().StartServer(ServerActivity.this,
                        ((RadioButton) findViewById(R.id.activity_server_connection_type_radio_wifi)).isChecked() ?
                                ConnectionType.CONNECTION_TYPE_WIFI
                                : ConnectionType.CONNECTION_TYPE_BLUETOOTH,
                        ((RadioButton) findViewById(R.id.activity_server_connection_type_radio_download)).isChecked() ?
                                ShareMethod.SHARE_METHOD_DIRECT_DOWNLOAD
                                : ShareMethod.SHARE_METHOD_STREAMING,
                        snackbar);

                if(!((RadioButton) findViewById(R.id.activity_server_connection_type_radio_wifi)).isChecked()) {
                    // disable button
                    ServerActivity.this.EditConnectionButton(false);
                }
            }
        });

    }

    // on start
    @Override
    public void onStart() {
        // parent start
        super.onStart();

        // register broadcast receiver
        this.broadcastReceiver = new BroadcastReceiverActivity();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("broadcastServerManagerMessage");
        registerReceiver(this.broadcastReceiver,
                intentFilter);
    }

    // on stop
    @Override
    public void onStop() {
        // parent stop
        super.onStop();

        // kill all clients
        BluetoothServerManager.GetInstance().KillAll();

        // unregister receiver
        super.unregisterReceiver(this.broadcastReceiver);
    }

    // on resume
    @Override
    public void onResume() {
        // parent resume
        super.onResume();
    }

    // on pause
    @Override
    public void onPause() {
        // parent pause
        super.onPause();
    }

    // on destroy
    @Override
    public void onDestroy() {
        // parent destroy
        super.onDestroy();
    }

    // catch event
    public void CatchEvent(int requestCode,
                           int resultCode) {
        // check request code
        if (requestCode >= RequestCode.values().length) {
            ClientManager.GetInstance().ResetConnection();
            return;
        }

        // process request
        switch (RequestCode.values()[requestCode]) {
            case REQUEST_CODE_ENABLE_BLUETOOTH:
                if (resultCode == RESULT_OK) {
                    // now waiting for devices
                    this.SetSnackbarMessage("Now waiting for devices...");
                    BluetoothServerManager.GetInstance().ActivateServer(this);
                } else {
                    this.EditConnectionButton(true);
                    this.SetClosableSnackbar("Bluetooth activation failed");
                }
                break;

            case REQUEST_CODE_BLUETOOTH_MAKE_DISCOVERABLE:
                if (resultCode <= 0) {
                    this.EditConnectionButton(true);
                    this.SetClosableSnackbar("Couldn't set discoverable mode");
                }
                break;

            default:
                break;
        }
    }

    // catch activity result
    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data) {
        CatchEvent(requestCode,
                resultCode);
    }

    // configure snackbar to be closable
    private void SetClosableSnackbar(String message) {
        this.snackbar.setText(message);
        this.snackbar.setAction("Close",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                }
        );
        this.snackbar.show();
    }

    // set snackbar message
    public void SetSnackbarMessage(String message) {
        this.snackbar.setText(message);
        this.snackbar.setAction("Close",
                null);
    }

    // disable button
    public void EditConnectionButton(boolean isEnabled) {
        FloatingActionButton button = findViewById(R.id.server_activity_connect_floating_button);
        button.setEnabled(isEnabled);
    }
}
